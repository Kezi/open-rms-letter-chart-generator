FROM debian:buster-slim as builder
WORKDIR /app
RUN apt-get update && apt-get install -y libssl-dev ca-certificates sshpass python3-pip python3-matplotlib git && rm -rf /var/lib/apt/lists/*

COPY main.py main.py
RUN python3 main.py


FROM caddy
WORKDIR /app
COPY --from=builder /app/rmsvotes.png /app/rmsvotes.png
CMD ["caddy", "file-server"]
