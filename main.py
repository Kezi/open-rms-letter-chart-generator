# author: Kezi
# license: gplv3

import time
import os
import subprocess
from threading import Thread

from datetime import datetime

import matplotlib.pyplot as plt
import matplotlib.dates as md


start_time = 1616522054+60*60*4
stop_time = int(time.time())

path_open = "/tmp/rms-open-letter.github.io" # https://github.com/rms-open-letter/rms-open-letter.github.io.git
path_support = "/tmp/rms-support-letter.github.io" # https://github.com/rms-support-letter/rms-support-letter.github.io.git

subprocess.check_output(['sh', '-c', f"rm -rf {path_open}"])
subprocess.check_output(['sh', '-c', f"rm -rf {path_support}"])

subprocess.check_output(['sh', '-c', f"git clone https://github.com/rms-open-letter/rms-open-letter.github.io.git {path_open}"])
subprocess.check_output(['sh', '-c', f"git clone https://github.com/rms-support-letter/rms-support-letter.github.io.git {path_support}"])


cwd = os.getcwd()

def count_open():
    lol=subprocess.check_output(['sh', '-c', f"cd {path_open}; cat index.md | grep -e \"^1.\" -e \"^- \" | wc -l"])
    return int(lol)

def count_support():
    lol=subprocess.check_output(['sh', '-c', f"cd {path_support}; ls _data/signed/ | wc -l"])
    return int(lol)

def checkout_open(date):
    try:
        subprocess.check_output(['sh', '-c', f"cd {path_open}; git checkout -f `git rev-list -n 1 --before=\"{date}\" main`"])
    except:
        pass

def checkout_support(date):
    try:
        subprocess.check_output(['sh', '-c', f"cd {path_support}; git checkout -f `git rev-list -n 1 --before=\"{date}\" master`"])
    except: 
        pass

def time_machine(timestamp):
    print("time machine", datetime.fromtimestamp(timestamp), timestamp)

    date=datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M')
    thread1 = Thread(target = checkout_open, args = (date, ))
    thread1.start()
    thread2 = Thread(target = checkout_support, args = (date, ))
    thread2.start()

    thread1.join()
    thread2.join()

def remove_spikes(arr):
    return [max(arr[i], arr[max(0,i-1)]) for i,x in enumerate(arr)] #I'm sorry for this, can't be bothered to do a proper fix


dates=[]
points_open=[]
points_support=[]

for i in range(start_time, stop_time, int((stop_time-start_time)/1000)):
    time_machine(i)

    dates.append(datetime.fromtimestamp(i))
    points_open.append(count_open())
    points_support.append(count_support())
    

plt.title('updated ' + datetime.utcfromtimestamp(stop_time).strftime('%Y-%m-%d %H:%M utc'))

plt.subplots_adjust(bottom=0.2)
plt.xticks(rotation=25)
ax = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)
plt.plot(dates, remove_spikes(points_open), label="rms remove letter")

plt.subplots_adjust(bottom=0.2)
plt.xticks(rotation=25)
ax = plt.gca()
xfmt = md.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(xfmt)
plt.plot(dates, remove_spikes(points_support), label="rms support letter")

plt.legend()

plt.savefig('rmsvotes.png', dpi=500)
